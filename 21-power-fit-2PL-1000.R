Fit_2PL <- function(path, n_samples = 1e3) {
    start_time <- Sys.time()
    library(ltm)
    library(doParallel)
    library(foreach)
     #Set number of cores for parallel computation
    registerDoParallel(cores = )
    
    setwd(path)
    
    models <-
        foreach(sample = 1:n_samples) %dopar% {
            cat(sprintf("\nFitting model %d of %d.", sample, n_samples))

            file_name <- paste ("X_3PL_", sample, ".dat", sep = "")
            data <- read.table(file_name)
            data <- data[1:1e3,]
            
            model <- ltm(data ~ z1)
            return(model)
        }

    finish_time <- Sys.time()
    total_time <- finish_time - start_time
    cat(sprintf("\n"))
    print(total_time)
    cat(sprintf("\nAll the models have been fitted, please wait until they are exported."))
    
    return(models)
}

#Set path to the input response matrices
twopl_threepl_1000 <-
    Fit_2PL("X_3PL")

#Set path to the output models
saveRDS(twopl_threepl_1000, file = "../models/twopl_threepl_1000.rds")
