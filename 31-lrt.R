library(ltm)

# Import the models
twopl_twopl_500      <-  readRDS("models/twopl_twopl_500.rds")
twopl_twopl_1000     <-  readRDS("models/twopl_twopl_1000.rds")
twopl_twopl_2500     <-  readRDS("models/twopl_twopl_2500.rds")
twopl_twopl_5000     <-  readRDS("models/twopl_twopl_5000.rds")
threepl_twopl_500    <-  readRDS("models/threepl_twopl_500.rds")
threepl_twopl_1000   <-  readRDS("models/threepl_twopl_1000.rds")
threepl_twopl_2500   <-  readRDS("models/threepl_twopl_2500.rds")
threepl_twopl_5000   <-  readRDS("models/threepl_twopl_5000.rds")
twopl_threepl_500    <-  readRDS("models/twopl_threepl_500.rds")
twopl_threepl_1000   <-  readRDS("models/twopl_threepl_1000.rds")
twopl_threepl_2500   <-  readRDS("models/twopl_threepl_2500.rds")
twopl_threepl_5000   <-  readRDS("models/twopl_threepl_5000.rds")
threepl_threepl_500  <-  readRDS("models/threepl_threepl_500.rds")
threepl_threepl_1000 <-  readRDS("models/threepl_threepl_1000.rds")
threepl_threepl_2500 <-  readRDS("models/threepl_threepl_2500.rds")
threepl_threepl_5000 <-  readRDS("models/threepl_threepl_5000.rds")


# Initialize empty vector and matrices
type1_lr_5000 <- vector("numeric", 1e3)
type1_pvalues_5000 <- matrix(NA, 1e3, 8)
type1_dof_5000 <- matrix(NA, 1e3, 7)

# For each sample per model
for(n_sample in 1:1000){
  # Get the likelihoods
  L0 <- twopl_twopl_5000[[n_sample]]$log.Lik
  L1 <- threepl_twopl_5000[[n_sample]]$log.Lik
  # Calculate the LRT statistic
  type1_lr_5000[n_sample] <- Q <- -2 * (L0 - L1)
  # Extract the guessing parameters
  cs <- coef(threepl_twopl_5000[[n_sample]])[,1]
  # Count how many parameters lie within the parameter space, given the threshold
  v <- c(
    sum(cs > 1e-2) - sum(cs > (1-1e-2)),
    sum(cs > 1e-3) - sum(cs > (1-1e-3)),
    sum(cs > 1e-4) - sum(cs > (1-1e-4)),
    sum(cs > 1e-5) - sum(cs > (1-1e-5)),
    sum(cs > 1e-6) - sum(cs > (1-1e-6)),
    sum(cs > 1e-7) - sum(cs > (1-1e-7)),
    sum(cs > 1e-8) - sum(cs > (1-1e-8))
  )
  # Save the estimated degrees of freedom, given the threshold
  type1_dof_5000[n_sample,]  <- v
  # Save the p-values, given the threshold 
  type1_pvalues_5000[n_sample, 1] <- pchisq(Q, v[1], lower.tail = FALSE)
  type1_pvalues_5000[n_sample, 2] <- pchisq(Q, v[2], lower.tail = FALSE)
  type1_pvalues_5000[n_sample, 3] <- pchisq(Q, v[3], lower.tail = FALSE)
  type1_pvalues_5000[n_sample, 4] <- pchisq(Q, v[4], lower.tail = FALSE)
  type1_pvalues_5000[n_sample, 5] <- pchisq(Q, v[5], lower.tail = FALSE)
  type1_pvalues_5000[n_sample, 6] <- pchisq(Q, v[6], lower.tail = FALSE)
  type1_pvalues_5000[n_sample, 7] <- pchisq(Q, v[7], lower.tail = FALSE)
  # When the threshold is 0, the CLRT is the naïve LRT
  type1_pvalues_5000[n_sample, 8] <- pchisq(Q, 30, lower.tail = FALSE)
}


type1_lr_2500 <- vector("numeric", 1e3)
type1_pvalues_2500 <- matrix(NA, 1e3, 8)
type1_dof_2500 <- matrix(NA, 1e3, 7)

for(n_sample in 1:1000){
    L0 <- twopl_twopl_2500[[n_sample]]$log.Lik
    L1 <- threepl_twopl_2500[[n_sample]]$log.Lik
    type1_lr_2500[n_sample] <- Q <- -2 * (L0 - L1)
    cs <- coef(threepl_twopl_2500[[n_sample]])[,1]
    v <- c(
        sum(cs > 1e-2) - sum(cs > (1-1e-2)),
        sum(cs > 1e-3) - sum(cs > (1-1e-3)),
        sum(cs > 1e-4) - sum(cs > (1-1e-4)),
        sum(cs > 1e-5) - sum(cs > (1-1e-5)),
        sum(cs > 1e-6) - sum(cs > (1-1e-6)),
        sum(cs > 1e-7) - sum(cs > (1-1e-7)),
        sum(cs > 1e-8) - sum(cs > (1-1e-8))
    )

    type1_dof_2500[n_sample,]  <- v

    type1_pvalues_2500[n_sample, 1] <- pchisq(Q, v[1], lower.tail = FALSE)
    type1_pvalues_2500[n_sample, 2] <- pchisq(Q, v[2], lower.tail = FALSE)
    type1_pvalues_2500[n_sample, 3] <- pchisq(Q, v[3], lower.tail = FALSE)
    type1_pvalues_2500[n_sample, 4] <- pchisq(Q, v[4], lower.tail = FALSE)
    type1_pvalues_2500[n_sample, 5] <- pchisq(Q, v[5], lower.tail = FALSE)
    type1_pvalues_2500[n_sample, 6] <- pchisq(Q, v[6], lower.tail = FALSE)
    type1_pvalues_2500[n_sample, 7] <- pchisq(Q, v[7], lower.tail = FALSE)
    type1_pvalues_2500[n_sample, 8] <- pchisq(Q, 30, lower.tail = FALSE)
}


type1_lr_1000 <- vector("numeric", 1e3)
type1_pvalues_1000 <- matrix(NA, 1e3, 8)
type1_dof_1000 <- matrix(NA, 1e3, 7)

for(n_sample in 1:1000){
    L0 <- twopl_twopl_1000[[n_sample]]$log.Lik
    L1 <- threepl_twopl_1000[[n_sample]]$log.Lik
    type1_lr_1000[n_sample] <- Q <- -2 * (L0 - L1)
    cs <- coef(threepl_twopl_1000[[n_sample]])[,1]
    v <- c(
        sum(cs > 1e-2) - sum(cs > (1-1e-2)),
        sum(cs > 1e-3) - sum(cs > (1-1e-3)),
        sum(cs > 1e-4) - sum(cs > (1-1e-4)),
        sum(cs > 1e-5) - sum(cs > (1-1e-5)),
        sum(cs > 1e-6) - sum(cs > (1-1e-6)),
        sum(cs > 1e-7) - sum(cs > (1-1e-7)),
        sum(cs > 1e-8) - sum(cs > (1-1e-8))
    )

    type1_dof_1000[n_sample,] <- v

    
    type1_pvalues_1000[n_sample, 1] <- pchisq(Q, v[1], lower.tail = FALSE)
    type1_pvalues_1000[n_sample, 2] <- pchisq(Q, v[2], lower.tail = FALSE)
    type1_pvalues_1000[n_sample, 3] <- pchisq(Q, v[3], lower.tail = FALSE)
    type1_pvalues_1000[n_sample, 4] <- pchisq(Q, v[4], lower.tail = FALSE)
    type1_pvalues_1000[n_sample, 5] <- pchisq(Q, v[5], lower.tail = FALSE)
    type1_pvalues_1000[n_sample, 6] <- pchisq(Q, v[6], lower.tail = FALSE)
    type1_pvalues_1000[n_sample, 7] <- pchisq(Q, v[7], lower.tail = FALSE)
    type1_pvalues_1000[n_sample, 8] <- pchisq(Q, 30, lower.tail = FALSE)
}

type1_lr_500 <- vector("numeric", 1e3)
type1_pvalues_500 <- matrix(NA, 1e3, 8)
type1_dof_500 <- matrix(NA, 1e3, 7)

for(n_sample in 1:1000){
    L0 <- twopl_twopl_500[[n_sample]]$log.Lik
    L1 <- threepl_twopl_500[[n_sample]]$log.Lik
    type1_lr_500[n_sample] <- Q <- -2 * (L0 - L1)
    cs <- coef(threepl_twopl_500[[n_sample]])[,1]
    v <- c(
        sum(cs > 1e-2) - sum(cs > (1-1e-2)),
        sum(cs > 1e-3) - sum(cs > (1-1e-3)),
        sum(cs > 1e-4) - sum(cs > (1-1e-4)),
        sum(cs > 1e-5) - sum(cs > (1-1e-5)),
        sum(cs > 1e-6) - sum(cs > (1-1e-6)),
        sum(cs > 1e-7) - sum(cs > (1-1e-7)),
        sum(cs > 1e-8) - sum(cs > (1-1e-8))
    )
    type1_dof_500[n_sample,] <- v
    type1_pvalues_500[n_sample, 1] <- pchisq(Q, v[1], lower.tail = FALSE)
    type1_pvalues_500[n_sample, 2] <- pchisq(Q, v[2], lower.tail = FALSE)
    type1_pvalues_500[n_sample, 3] <- pchisq(Q, v[3], lower.tail = FALSE)
    type1_pvalues_500[n_sample, 4] <- pchisq(Q, v[4], lower.tail = FALSE)
    type1_pvalues_500[n_sample, 5] <- pchisq(Q, v[5], lower.tail = FALSE)
    type1_pvalues_500[n_sample, 6] <- pchisq(Q, v[6], lower.tail = FALSE)
    type1_pvalues_500[n_sample, 7] <- pchisq(Q, v[7], lower.tail = FALSE)
    type1_pvalues_500[n_sample, 8] <- pchisq(Q, 30, lower.tail = FALSE)
}


power_lr_5000 <- vector("numeric", 1e3)
power_pvalues_5000 <- matrix(NA, 1e3, 8)
power_dof_5000 <- matrix(NA, 1e3, 7)

for(n_sample in 1:1000){
    L0 <- twopl_threepl_5000[[n_sample]]$log.Lik
    L1 <- threepl_threepl_5000[[n_sample]]$log.Lik
    power_lr_5000[n_sample] <- Q <- -2 * (L0 - L1)
    cs <- coef(threepl_threepl_5000[[n_sample]])[,1]

    v <- c(
        sum(cs > 1e-2) - sum(cs > (1-1e-2)),
        sum(cs > 1e-3) - sum(cs > (1-1e-3)),
        sum(cs > 1e-4) - sum(cs > (1-1e-4)),
        sum(cs > 1e-5) - sum(cs > (1-1e-5)),
        sum(cs > 1e-6) - sum(cs > (1-1e-6)),
        sum(cs > 1e-7) - sum(cs > (1-1e-7)),
        sum(cs > 1e-8) - sum(cs > (1-1e-8))
    )

    power_dof_5000[n_sample,] <- v

    power_pvalues_5000[n_sample, 1] <- pchisq(Q, v[1], lower.tail = FALSE)
    power_pvalues_5000[n_sample, 2] <- pchisq(Q, v[2], lower.tail = FALSE)
    power_pvalues_5000[n_sample, 3] <- pchisq(Q, v[3], lower.tail = FALSE)
    power_pvalues_5000[n_sample, 4] <- pchisq(Q, v[4], lower.tail = FALSE)
    power_pvalues_5000[n_sample, 5] <- pchisq(Q, v[5], lower.tail = FALSE)
    power_pvalues_5000[n_sample, 6] <- pchisq(Q, v[6], lower.tail = FALSE)
    power_pvalues_5000[n_sample, 7] <- pchisq(Q, v[7], lower.tail = FALSE)
    power_pvalues_5000[n_sample, 8] <- pchisq(Q, 30, lower.tail = FALSE)
}

power_lr_2500 <- vector("numeric", 1e3)
power_pvalues_2500 <- matrix(NA, 1e3, 8)
power_dof_2500 <- matrix(NA, 1e3, 7)
for(n_sample in 1:1000){
    L0 <- twopl_threepl_2500[[n_sample]]$log.Lik
    L1 <- threepl_threepl_2500[[n_sample]]$log.Lik
    power_lr_2500[n_sample] <- Q <- -2 * (L0 - L1)
    cs <- coef(threepl_threepl_2500[[n_sample]])[,1]
    v <- c(
        sum(cs > 1e-2) - sum(cs > (1-1e-2)),
        sum(cs > 1e-3) - sum(cs > (1-1e-3)),
        sum(cs > 1e-4) - sum(cs > (1-1e-4)),
        sum(cs > 1e-5) - sum(cs > (1-1e-5)),
        sum(cs > 1e-6) - sum(cs > (1-1e-6)),
        sum(cs > 1e-7) - sum(cs > (1-1e-7)),
        sum(cs > 1e-8) - sum(cs > (1-1e-8))
    )
    power_dof_2500[n_sample,] <- v

    power_pvalues_2500[n_sample, 1] <- pchisq(Q, v[1], lower.tail = FALSE)
    power_pvalues_2500[n_sample, 2] <- pchisq(Q, v[2], lower.tail = FALSE)
    power_pvalues_2500[n_sample, 3] <- pchisq(Q, v[3], lower.tail = FALSE)
    power_pvalues_2500[n_sample, 4] <- pchisq(Q, v[4], lower.tail = FALSE)
    power_pvalues_2500[n_sample, 5] <- pchisq(Q, v[5], lower.tail = FALSE)
    power_pvalues_2500[n_sample, 6] <- pchisq(Q, v[6], lower.tail = FALSE)
    power_pvalues_2500[n_sample, 7] <- pchisq(Q, v[7], lower.tail = FALSE)
    power_pvalues_2500[n_sample, 8] <- pchisq(Q, 30, lower.tail = FALSE)
}

power_lr_1000 <- vector("numeric", 1e3)
power_pvalues_1000 <- matrix(NA, 1e3, 8)
power_dof_1000 <- matrix(NA, 1e3, 7)
for(n_sample in 1:1000){
    L0 <- twopl_threepl_1000[[n_sample]]$log.Lik
    L1 <- threepl_threepl_1000[[n_sample]]$log.Lik
    power_lr_1000[n_sample] <- Q <- -2 * (L0 - L1)
    cs <- coef(threepl_threepl_1000[[n_sample]])[,1]
    v <- c(
        sum(cs > 1e-2) - sum(cs > (1-1e-2)),
        sum(cs > 1e-3) - sum(cs > (1-1e-3)),
        sum(cs > 1e-4) - sum(cs > (1-1e-4)),
        sum(cs > 1e-5) - sum(cs > (1-1e-5)),
        sum(cs > 1e-6) - sum(cs > (1-1e-6)),
        sum(cs > 1e-7) - sum(cs > (1-1e-7)),
        sum(cs > 1e-8) - sum(cs > (1-1e-8))
    )

    power_dof_1000[n_sample,] <- v

    power_pvalues_1000[n_sample, 1] <- pchisq(Q, v[1], lower.tail = FALSE)
    power_pvalues_1000[n_sample, 2] <- pchisq(Q, v[2], lower.tail = FALSE)
    power_pvalues_1000[n_sample, 3] <- pchisq(Q, v[3], lower.tail = FALSE)
    power_pvalues_1000[n_sample, 4] <- pchisq(Q, v[4], lower.tail = FALSE)
    power_pvalues_1000[n_sample, 5] <- pchisq(Q, v[5], lower.tail = FALSE)
    power_pvalues_1000[n_sample, 6] <- pchisq(Q, v[6], lower.tail = FALSE)
    power_pvalues_1000[n_sample, 7] <- pchisq(Q, v[7], lower.tail = FALSE)
    power_pvalues_1000[n_sample, 8] <- pchisq(Q, 30, lower.tail = FALSE)
}


power_lr_500 <- vector("numeric", 1e3)
power_pvalues_500 <- matrix(NA, 1e3, 8)
power_dof_500 <- matrix(NA, 1e3, 7)
for(n_sample in 1:1000){
    L0 <- twopl_threepl_500[[n_sample]]$log.Lik
    L1 <- threepl_threepl_500[[n_sample]]$log.Lik
    power_lr_500[n_sample] <- Q <- -2 * (L0 - L1)
    cs <- coef(threepl_threepl_500[[n_sample]])[,1]
    v <- c(
        sum(cs > 1e-2) - sum(cs > (1-1e-2)),
        sum(cs > 1e-3) - sum(cs > (1-1e-3)),
        sum(cs > 1e-4) - sum(cs > (1-1e-4)),
        sum(cs > 1e-5) - sum(cs > (1-1e-5)),
        sum(cs > 1e-6) - sum(cs > (1-1e-6)),
        sum(cs > 1e-7) - sum(cs > (1-1e-7)),
        sum(cs > 1e-8) - sum(cs > (1-1e-8))
    )
    power_dof_500[n_sample,] <- v

    power_pvalues_500[n_sample, 1] <- pchisq(Q, v[1], lower.tail = FALSE)
    power_pvalues_500[n_sample, 2] <- pchisq(Q, v[2], lower.tail = FALSE)
    power_pvalues_500[n_sample, 3] <- pchisq(Q, v[3], lower.tail = FALSE)
    power_pvalues_500[n_sample, 4] <- pchisq(Q, v[4], lower.tail = FALSE)
    power_pvalues_500[n_sample, 5] <- pchisq(Q, v[5], lower.tail = FALSE)
    power_pvalues_500[n_sample, 6] <- pchisq(Q, v[6], lower.tail = FALSE)
    power_pvalues_500[n_sample, 7] <- pchisq(Q, v[7], lower.tail = FALSE)
    power_pvalues_500[n_sample, 8] <- pchisq(Q, 30, lower.tail = FALSE)
}

save(
    power_lr_500,
    power_lr_1000,
    power_lr_2500,
    power_lr_5000,
    type1_lr_500,
    type1_lr_1000,
    type1_lr_2500,
    type1_lr_5000,

    power_pvalues_500,
    power_pvalues_1000,
    power_pvalues_2500,
    power_pvalues_5000,
    type1_pvalues_500,
    type1_pvalues_1000,
    type1_pvalues_2500,
    type1_pvalues_5000,

    power_dof_500,
    power_dof_1000,
    power_dof_2500,
    power_dof_5000,
    type1_dof_500,
    type1_dof_1000,
    type1_dof_2500,
    type1_dof_5000,

    file = "raw_results.RData"
)

quit(save = "no")
